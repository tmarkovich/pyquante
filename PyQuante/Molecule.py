"""\
 Molecule.py: Simple class for molecules.

 TODO: *Really* need to think of a more intelligent way of handling
 units!

 This program is part of the PyQuante quantum chemistry program suite.

 Copyright (c) 2004, Richard P. Muller. All Rights Reserved.

 PyQuante version 1.2 and later is covered by the modified BSD
 license. Please see the file LICENSE that is part of this
 distribution.
"""
from . import settings
from PyQuante.Atom import Atom
from PyQuante.Element import sym2no
from PyQuante.Constants import bohr2ang, ang2bohr

from PyQuante.settings import openbabel_enabled

if openbabel_enabled:
    from PyQuante.IO.OpenBabelBackend import BabelFileHandler as FileHandler
    from PyQuante.IO.OpenBabelBackend import BabelStringHandler as StringHandler
else:
    from PyQuante.IO.PyQuanteBackend import PyQuanteStringHandler as StringHandler
    from PyQuante.IO.PyQuanteBackend import PyQuanteFileHandler as FileHandler

allowed_units = ['bohr', 'angs']


colors_dict = {
    'automatic': '#add8e6',     # 173, 216, 230
    'aliceblue': '#f0f8ff',     # 240, 248, 255
    'antiquewhite': '#faebd7',     # 250, 235, 215
    'aqua': '#00ffff',  # 0, 255, 255
    'aquamarine': '#7fffd4',     # 127, 255, 212
    'azure': '#f0ffff',     # 240, 255, 255
    'beige': '#f5f5dc',     # 245, 245, 220
    'bisque': '#ffe4c4',     # 255, 228, 196
    'black': '#000000',  # 0,   0,   0
    'blanchedalmond': '#ffebcd',     # 255, 235, 205
    'blue': '#0000ff',  # 0,   0, 255
    'blueviolet': '#8a2be2',     # 138,  43, 226
    'brown': '#a52a2a',     # 165,  42,  42
    'burlywood': '#deb887',     # 222, 184, 135
    'cadetblue': '#5f9ea0',  # 95, 158, 160
    'chartreuse': '#7fff00',     # 127, 255,   0
    'chocolate': '#d2691e',     # 210, 105,  30
    'coral': '#ff7f50',     # 255, 127,  80
    'cornflowerblue': '#6495ed',     # 100, 149, 237
    'cornsilk': '#fff8dc',     # 255, 248, 220
    'crimson': '#dc143c',     # 220,  20,  60
    'cyan': '#00ffff',  # 0, 255, 255
    'darkblue': '#00008b',  # 0,   0, 139
    'darkcyan': '#008b8b',  # 0, 139, 139
    'darkgoldenrod': '#b8860b',     # 184, 134,  11
    'darkgray': '#a9a9a9',     # 169, 169, 169
    'darkgreen': '#006400',  # 0, 100,   0
    'darkgrey': '#a9a9a9',     # 169, 169, 169
    'darkkhaki': '#bdb76b',     # 189, 183, 107
    'darkmagenta': '#8b008b',     # 139,   0, 139
    'darkolivegreen': '#556b2f',  # 85, 107,  47
    'darkorange': '#ff8c00',     # 255, 140,   0
    'darkorchid': '#9932cc',     # 153,  50, 204
    'darkred': '#8b0000',     # 139,   0,   0
    'darksalmon': '#e9967a',     # 233, 150, 122
    'darkseagreen': '#8fbc8f',     # 143, 188, 143
    'darkslateblue': '#483d8b',  # 72,  61, 139
    'darkslategray': '#2f4f4f',  # 47,  79,  79
    'darkslategrey': '#2f4f4f',  # 47,  79,  79
    'darkturquoise': '#00ced1',  # 0, 206, 209
    'darkviolet': '#9400d3',     # 148,   0, 211
    'deeppink': '#ff1493',     # 255,  20, 147
    'deepskyblue': '#00bfff',  # 0, 191, 255
    'dimgray': '#696969',     # 105, 105, 105
    'dimgrey': '#696969',     # 105, 105, 105
    'dodgerblue': '#1e90ff',  # 30, 144, 255
    'firebrick': '#b22222',     # 178,  34,  34
    'floralwhite': '#fffaf0',     # 255, 250, 240
    'forestgreen': '#228b22',  # 34, 139,  34
    'fuchsia': '#ff00ff',     # 255,   0, 255
    'gainsboro': '#dcdcdc',     # 220, 220, 220
    'ghostwhite': '#f8f8ff',     # 248, 248, 255
    'gold': '#ffd700',     # 255, 215,   0
    'goldenrod': '#daa520',     # 218, 165,  32
    'gray': '#808080',     # 128, 128, 128
    'green': '#008000',  # 0, 128,   0
    'greenyellow': '#adff2f',     # 173, 255,  47
    'grey': '#808080',     # 128, 128, 128
    'honeydew': '#f0fff0',     # 240, 255, 240
    'hotpink': '#ff69b4',     # 255, 105, 180
    'indianred': '#cd5c5c',     # 205,  92,  92
    'indigo': '#4b0082',  # 75,   0, 130
    'ivory': '#fffff0',     # 255, 255, 240
    'khaki': '#f0e68c',     # 240, 230, 140
    'lavender': '#e6e6fa',     # 230, 230, 250
    'lavenderblush': '#fff0f5',     # 255, 240, 245
    'lawngreen': '#7cfc00',     # 124, 252,   0
    'lemonchiffon': '#fffacd',     # 255, 250, 205
    'lightblue': '#add8e6',     # 173, 216, 230
    'lightcoral': '#f08080',     # 240, 128, 128
    'lightcyan': '#e0ffff',     # 224, 255, 255
    'lightgoldenrodyellow': '#fafad2',     # 250, 250, 210
    'lightgray': '#d3d3d3',     # 211, 211, 211
    'lightgreen': '#90ee90',     # 144, 238, 144
    'lightgrey': '#d3d3d3',     # 211, 211, 211
    'lightpink': '#ffb6c1',     # 255, 182, 193
    'lightsalmon': '#ffa07a',     # 255, 160, 122
    'lightseagreen': '#20b2aa',  # 32, 178, 170
    'lightskyblue': '#87cefa',     # 135, 206, 250
    'lightslategray': '#778899',     # 119, 136, 153
    'lightslategrey': '#778899',     # 119, 136, 153
    'lightsteelblue': '#b0c4de',     # 176, 196, 222
    'lightyellow': '#ffffe0',     # 255, 255, 224
    'lime': '#00ff00',  # 0, 255,   0
    'limegreen': '#32cd32',  # 50, 205,  50
    'linen': '#faf0e6',     # 250, 240, 230
    'magenta': '#ff00ff',     # 255,   0, 255
    'maroon': '#800000',     # 128,   0,   0
    'mediumaquamarine': '#66cdaa',     # 102, 205, 170
    'mediumblue': '#0000cd',  # 0,   0, 205
    'mediumorchid': '#ba55d3',     # 186,  85, 211
    'mediumpurple': '#9370db',     # 147, 112, 219
    'mediumseagreen': '#3cb371',  # 60, 179, 113
    'mediumslateblue': '#7b68ee',     # 123, 104, 238
    'mediumspringgreen': '#00fa9a',  # 0, 250, 154
    'mediumturquoise': '#48d1cc',  # 72, 209, 204
    'mediumvioletred': '#c71585',     # 199,  21, 133
    'midnightblue': '#191970',  # 25,  25, 112
    'mintcream': '#f5fffa',     # 245, 255, 250
    'mistyrose': '#ffe4e1',     # 255, 228, 225
    'moccasin': '#ffe4b5',     # 255, 228, 181
    'navajowhite': '#ffdead',     # 255, 222, 173
    'navy': '#000080',  # 0,   0, 128
    'oldlace': '#fdf5e6',     # 253, 245, 230
    'olive': '#808000',     # 128, 128,   0
    'olivedrab': '#6b8e23',     # 107, 142,  35
    'orange': '#ffa500',     # 255, 165,   0
    'orangered': '#ff4500',     # 255,  69,   0
    'orchid': '#da70d6',     # 218, 112, 214
    'palegoldenrod': '#eee8aa',     # 238, 232, 170
    'palegreen': '#98fb98',     # 152, 251, 152
    'paleturquoise': '#afeeee',     # 175, 238, 238
    'palevioletred': '#db7093',     # 219, 112, 147
    'papayawhip': '#ffefd5',     # 255, 239, 213
    'peachpuff': '#ffdab9',     # 255, 218, 185
    'peru': '#cd853f',     # 205, 133,  63
    'pink': '#ffc0cb',     # 255, 192, 203
    'plum': '#dda0dd',     # 221, 160, 221
    'powderblue': '#b0e0e6',     # 176, 224, 230
    'purple': '#800080',     # 128,   0, 128
    'red': '#ff0000',     # 255,   0,   0
    'rosybrown': '#bc8f8f',     # 188, 143, 143
    'royalblue': '#4169e1',  # 65, 105, 225
    'saddlebrown': '#8b4513',     # 139,  69,  19
    'salmon': '#fa8072',     # 250, 128, 114
    'sandybrown': '#f4a460',     # 244, 164,  96
    'seagreen': '#2e8b57',  # 46, 139,  87
    'seashell': '#fff5ee',     # 255, 245, 238
    'sienna': '#a0522d',     # 160,  82,  45
    'silver': '#c0c0c0',     # 192, 192, 192
    'skyblue': '#87ceeb',     # 135, 206, 235
    'slateblue': '#6a5acd',     # 106,  90, 205
    'slategray': '#708090',     # 112, 128, 144
    'slategrey': '#708090',     # 112, 128, 144
    'snow': '#fffafa',     # 255, 250, 250
    'springgreen': '#00ff7f',  # 0, 255, 127
    'steelblue': '#4682b4',  # 70, 130, 180
    'tan': '#d2b48c',     # 210, 180, 140
    'teal': '#008080',  # 0, 128, 128
    'thistle': '#d8bfd8',     # 216, 191, 216
    'tomato': '#ff6347',     # 255,  99,  71
    'turquoise': '#40e0d0',  # 64, 224, 208
    'violet': '#ee82ee',     # 238, 130, 238
    'wheat': '#f5deb3',     # 245, 222, 179
    'white': '#ffffff',     # 255, 255, 255
    'whitesmoke': '#f5f5f5',     # 245, 245, 245
    'yellow': '#ffff00',     # 255, 255,   0
    'yellowgreen': '#9acd32'      # 154, 205,  50
}


def html_to_float(c):
    """

    Taken from Sage color.py

    Convert a HTML hex color to a Red-Green-Blue (RGB) tuple.

    INPUT:

    - ``c`` - a string; a valid HTML hex color

    OUTPUT:

    - a RGB 3-tuple of floats in the interval [0.0, 1.0]

    EXAMPLES::

        sage: from sage.plot.colors import html_to_float
        sage: html_to_float('#fff')
        (1.0, 1.0, 1.0)
        sage: html_to_float('#abcdef')
        (0.6705882352941176, 0.803921568627451, 0.9372549019607843)
        sage: html_to_float('#123xyz')
        Traceback (most recent call last):
        ...
        ValueError: invalid literal for int() with base 16: '3x'
    """
    if not len(c) or c[0] != '#':
        raise ValueError(
            "'%s' must be a valid HTML hex color (e.g., '#f07' or '#d6e7da')" %
            c)
    h = c[1:]
    if len(h) == 3:
        h = '%s%s%s%s%s%s' % (h[0], h[0], h[1], h[1], h[2], h[2])
    elif len(h) != 6:
        raise ValueError(
            "color hex string (= '%s') must have length 3 or 6" %
            h)
    return tuple([int(h[i:i + 2], base=16) / float(255) for i in [0, 2, 4]])


class Molecule:

    """\
    Molecule(name,atomlist,**kwargs) - Object to hold molecular data in PyQuante

    name       The name of the molecule
    atomlist   A list of (atno,(x,y,z)) information for the molecule

    Options:      Value   Description
    --------      -----   -----------
    units         Bohr    Units for the coordinates of the molecule
    charge        0       The molecular charge
    multiplicity  1       The spin multiplicity of the molecule
    """

    def __init__(self, name='molecule', atomlist=[], **kwargs):
        self.name = name
        self.atoms = []
        self.basis = []
        self.grid = None
        units = kwargs.get('units', settings.LengthUnits)
        units = units.lower()[:4]
        assert units in allowed_units
        self.units = units
        if atomlist:
            self.add_atuples(atomlist)
        self.charge = int(kwargs.get('charge', settings.MolecularCharge))
        self.multiplicity = int(
            kwargs.get(
                'multiplicity',
                settings.SpinMultiplicity))
        return
    # Alternative constructors
    # @classmethod <- python2.4

    def from_file(cls, filename, format=None, name="molecule"):
        hand = FileHandler()
        data = hand.read(filename, format)
        atomlist = [(at.atno, at.r) for at in data.molecule.atoms]
        return cls(name, atomlist=atomlist,
                   charge=data.molecule.charge,
                   multiplicity=data.molecule.multiplicity)
    from_file = classmethod(from_file)  # old decorator syntax

    def from_string(cls, string, format, name="molecule"):
        hand = StringHandler()
        data = hand.read(string, format)
        atomlist = [(at.atno, at.r) for at in data.molecule.atoms]
        return cls(name, atomlist=atomlist,
                   charge=data.molecule.charge,
                   multiplicity=data.molecule.multiplicity)
    from_string = classmethod(from_string)  # old decorator syntax

    def as_string(self, format="xyz"):
        from PyQuante.IO.Data import Data
        data = Data()
        data.molecule = self
        hand = StringHandler()
        return hand.write(data, format)

    def dump(self, filename, format=None):
        from PyQuante.IO.Data import Data
        data = Data()
        data.molecule = self
        hand = FileHandler()
        hand.write(filename, data, format)

    def __repr__(self):
        outl = "\n%s: %s" % (self.__class__.__name__, self.name)
        for atom in self.atoms:
            outl += "\n\t%s" % str(atom)
        return outl

    def update_from_atuples(self, geo):
        nat = len(geo)
        assert nat == len(self.atoms)
        for i in xrange(nat):
            self.atoms[i].update_from_atuple(geo[i])
        return

    def update_coords(self, coords):
        nat = len(coords) / 3
        assert nat == len(self.atoms)
        for i in xrange(nat):
            self.atoms[i].update_coords(coords[3 * i:3 * i + 3])
        return

    def translate(self, pos):
        for atom in self.atoms:
            atom.translate(pos)
        return

    def add_atom(self, atom):
        self.atoms.append(atom)

    def add_atuple(self, atno, xyz, atid):
        if self.units != 'bohr':
            xyz = toBohr(xyz[0], xyz[1], xyz[2])
        if isinstance(atno, type('')):
            atno = sym2no[atno]
        self.atoms.append(Atom(atno, xyz[0], xyz[1], xyz[2], atid))

    def add_atuples(self, atoms):
        "Add a list of (atno,(x,y,z)) tuples to the atom list"
        from .Atom import Atom
        for id, (atno, xyz) in enumerate(atoms):
            self.add_atuple(atno, xyz, id)
            id += 1
        return

    def atuples(self):
        "Express molecule as a list of (atno,(x,y,z)) tuples"
        atoms = []
        for atom in self.atoms:
            atoms.append(atom.atuple())
        return atoms

    def atuples_angstrom(self):
        atoms = []
        for atom in self.atoms:
            atno, xyz = atom.atuple()
            atoms.append((atno, toAng(xyz[0], xyz[1], xyz[2])))
        return atoms

    # Not really used. Consider removing
    def add_xyz_file(self, filename, which_frame=-1):
        "Input atoms from xyz file. By default choose the last frame"
        from .IO import read_xyz
        geos = read_xyz(filename)
        self.add_atuples(geos[which_frame])
        return

    def set_charge(self, charge):
        self.charge = int(charge)

    def get_charge(self):
        return self.charge

    def set_multiplicity(self, mult):
        self.multiplicity = int(mult)

    def get_multiplicity(self):
        return self.multiplicity

    def get_nel(self, charge=None):
        if charge:
            # Deprecation warning inserted 8/2005
            print "Warning: use of get_nel(charge) has been deprecated"
            print "Please supply charge at construction of molecule or use"
            print "mol.set_charge(charge)"
            self.set_charge(charge)
        nel = -self.charge
        for atom in self.atoms:
            nel += atom.get_nel()
        return nel

    def get_enuke(self):
        enuke = 0.
        nat = len(self.atoms)
        for i in xrange(nat):
            ati = self.atoms[i]
            for j in xrange(i):
                atj = self.atoms[j]
                enuke += ati.get_nuke_chg() * atj.get_nuke_chg() / \
                    ati.dist(atj)
        return enuke

    def get_closedopen(self):
        multiplicity = self.multiplicity
        nel = self.get_nel()

        assert multiplicity > 0

        if (nel % 2 == 0 and multiplicity % 2 == 0) \
                or (nel % 2 == 1 and multiplicity % 2 == 1):
            print "Incompatible number of electrons and spin multiplicity"
            print "nel = ", nel
            print "multiplicity = ", multiplicity
            raise Exception(
                "Incompatible number of electrons and spin multiplicity")

        nopen = multiplicity - 1
        nclosed, ierr = divmod(nel - nopen, 2)
        if ierr:
            print "Error in Molecule.get_closedopen()"
            print 'nel = ', nel
            print 'multiplicity = ', multiplicity
            print 'nopen = ', nopen
            print 'nclosed = ', nclosed
            print 'ierr = ', ierr
            raise Exception("Error in Molecule.get_closedopen()")
        return nclosed, nopen

    def get_alphabeta(self, **kwargs):
        nclosed, nopen = self.get_closedopen(**kwargs)
        return nclosed + nopen, nclosed

    def com(self):
        "Compute the center of mass of the molecule"
        from PyQuante.NumWrap import zeros
        rcom = zeros((3,), 'd')
        mtot = 0
        for atom in self:
            m = atom.mass()
            rcom += m * atom.r
            mtot += m
        rcom /= mtot
        return rcom

    def inertial(self):
        "Transform to inertial coordinates"
        from PyQuante.NumWrap import zeros, eigh
        rcom = self.com()
        print "Translating to COM: ", rcom
        self.translate(-rcom)
        I = zeros((3, 3), 'd')
        for atom in self:
            m = atom.mass()
            x, y, z = atom.pos()
            x2, y2, z2 = x * x, y * y, z * z
            I[0, 0] += m * (y2 + z2)
            I[1, 1] += m * (x2 + z2)
            I[2, 2] += m * (x2 + y2)
            I[0, 1] -= m * x * y
            I[1, 0] = I[0, 1]
            I[0, 2] -= m * x * z
            I[2, 0] = I[0, 2]
            I[1, 2] -= m * y * z
            I[2, 1] = I[1, 2]
        E, U = eigh(I)
        print "Moments of inertial ", E
        self.urotate(U)
        print "New coordinates: "
        print self
        return

    def urotate(self, U):
        "Rotate molecule by the unitary matrix U"
        for atom in self:
            atom.urotate(U)
        return

    # These two overloads let the molecule act as a list of atoms
    def __getitem__(self, i):
        return self.atoms[i]

    def __len__(self):
        return len(self.atoms)

    # These overloads let one create a subsystem from a list of atoms
    def subsystem(self, name, indices, **kwargs):
        submol = Molecule(name, None, **kwargs)
        for i in indices:
            submol.add_atom(self.atoms[i])
        return submol

    def copy(self):
        import copy
        return copy.deepcopy(self)

    def xyz_file(self, fname=None):
        if not fname:
            fname = self.name + ".xyz"
        lines = ["%d\nWritten by PyQuante.Molecule" % len(self.atoms)]
        for atom in self:
            x, y, z = [bohr2ang * i for i in atom.pos()]
            lines.append(
                "%s %15.10f %15.10f %15.10f" %
                (atom.symbol(), x, y, z))
        open(fname, 'w').write("\n".join(lines))
        return

    def plot_orbs(self, orb, basis_set=None, contour=None):
        from mayavi import mlab
        from PyQuante.Ints import getbasis
        import numpy as np

        if basis_set is None:
            bfs = getbasis(self)
        else:
            bfs = getbasis(self, basis_data=basis_set)

        mlab.figure(1, bgcolor=(0, 0, 0), size=(750, 750))
        mlab.clf()

        xarray = np.zeros((len(self), ))
        yarray = np.zeros((len(self), ))
        zarray = np.zeros((len(self), ))
        color_dict = {1: html_to_float('#FFFFFF'),
                      2: html_to_float('#CCFFFF'),
                      3: html_to_float('#CC0099'),
                      4: html_to_float('#CCFF00'),
                      5: html_to_float('#FF9999'),
                      7: html_to_float('#CCCCCC'),
                      6: html_to_float('#3333FF'),
                      8: html_to_float('#FF3333'),
                      9: html_to_float('#9999FF'),
                      10: html_to_float('#99FFFF'),
                      11: html_to_float('#CC00CC'),
                      12: html_to_float('#99FF00'),
                      13: html_to_float('#333300'),
                      14: html_to_float('#336600'),
                      15: html_to_float('#FF9933'),
                      16: html_to_float('#996600'),
                      17: html_to_float('#66FF00'),
                      18: html_to_float('#00FFFF'),
                      35: html_to_float('#FFC0CB')}
        scale_dict = {1: 1,
                      2: 1.5,
                      3: 1.5,
                      4: 1.5,
                      5: 1.5,
                      6: 1.5,
                      7: 1.5,
                      8: 1.5,
                      9: 1.5,
                      10: 1.5,
                      11: 1.5,
                      12: 1.5,
                      13: 1.5,
                      14: 1.5,
                      15: 1.5,
                      16: 1.5,
                      17: 1.5,
                      18: 1.5,
                      35: 1.5}

        for i in range(len(self)):
            (xarray[i], yarray[i], zarray[i]) = self[i].r
            at = mlab.points3d(xarray[i], yarray[i], zarray[i],
                               scale_factor=scale_dict[self[i].atno],
                               resolution=20,
                               color=color_dict[self[i].atno],
                               scale_mode='none')

        x, y, z = np.mgrid[min(xarray) - 10.0:max(xarray) + 10.0:100j,
                           min(yarray) - 10.0:max(yarray) + 10.0:100j,
                           min(zarray) - 10.0:max(zarray) + 10.0:100j]
        fxyz = np.zeros((100, 100, 100))
        for ibf in range(len(bfs)):
            fxyz += orb[ibf] * bfs[ibf].amp(x, y, z)
        #fxyz = np.abs(fxyz)**2
        src = mlab.pipeline.scalar_field(x, y, z, fxyz)
        mlab.pipeline.iso_surface(
            src,
            contours=[
                0.08 *
                fxyz.ptp(),
            ],
            opacity=0.6,
            color=html_to_float('#0033CC'))
        mlab.pipeline.iso_surface(src,
                                  contours=[-0.08 * fxyz.ptp(),
                                            ],
                                  opacity=0.6,
                                  color=html_to_float('#FF0033'))
        mlab.show()


def toBohr(*args):
    if len(args) == 1:
        return ang2bohr * args[0]
    return [ang2bohr * arg for arg in args]


def toAng(*args):
    if len(args) == 1:
        return bohr2ang * args[0]
    return [bohr2ang * arg for arg in args]


def cleansym(s):
    import re
    return re.split('[^a-zA-z]', s)[0]


def ParseXYZLines(name, xyz_lines, **kwargs):
    atoms = []
    for line in xyz_lines.splitlines():
        words = line.split()
        if not words:
            continue
        sym = cleansym(words[0])
        xyz = map(float, words[1:4])
        atoms.append((sym, xyz))
    return Molecule(name, atoms, **kwargs)


def mol2mpqc(mol, **kwargs):
    from PyQuante.Element import symbol
    xc = kwargs.get('xc', 'B3LYP')
    basis = kwargs.get('basis', '6-31g**')
    lines = ['%% %s %s calculation with MPQC' % (mol.name, xc),
             'optimize: yes',
             'method: KS (xc=%s)' % xc,
             'basis: %s' % basis,
             'molecule:']
    for atom in mol:
        atno, xyz = atom.atuple()
        xyz = toAng(xyz)
        lines.append("   %4s %12.6f %12.6f %12.6f" %
                     (symbol[atno], xyz[0], xyz[1], xyz[2]))
    return "\n".join(lines)


def mol2xyz(mol, **kwargs):
    lines = ['%d\nXYZ File for %s' % (len(mol), mol.name)]
    for atom in mol:
        atno, xyz = atom.atuple()
        xyz = toAng(xyz)
        lines.append("   %4s %12.6f %12.6f %12.6f" %
                     (symbol[atno], xyz[0], xyz[1], xyz[2]))
    return "\n".join(lines)

if __name__ == '__main__':
    h2o = Molecule('h2o',
                   [('O', (0., 0., 0.)), ('H', (1., 0., 0.)),
                    ('H', (0., 1., 0.))],
                   units='Angstrom')
    print h2o
    print h2o.subsystem([0, 1])
