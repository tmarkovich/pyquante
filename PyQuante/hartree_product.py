"""\
 hartree_fock.py Basic routines for HF programs in the PyQuante framework

 This program is part of the PyQuante quantum chemistry program suite.

 Copyright (c) 2004, Richard P. Muller. All Rights Reserved.

 PyQuante version 1.2 and later is covered by the modified BSD
 license. Please see the file LICENSE that is part of this
 distribution.
"""

import string
import sys
import time
from . import settings
from .fermi_dirac import get_efermi, get_fermi_occs, mkdens_occs, get_entropy
from .LA2 import geigh, mkdens, trace2
from .Ints import get2JmK, getbasis, getints, fetch_jints
from numpy import reshape, zeros, dot, zeros_like
import numpy as np
from .Convergence import DIIS
import logging

logger = logging.getLogger("pyquante")  # Hack!!!

from math import sqrt, pow, fabs
from PyQuante.cints import dist

from .NumWrap import array2string, dot, transpose


def get_fock(D, Ints, h):
    "Return the HF Fock matrix"
    return h + get2JmK(Ints, D)


def get_energy(h, F, D, enuke=0., **kwargs):
    "Form the total energy of the closed-shell wave function."
    eone = trace2(D, h)
    etwo = trace2(D, F)
    # print "eone,etwo,enuke",(2*eone),(etwo-eone),enuke
    return eone + etwo + enuke


def get_guess(h, S):
    "Form an initial guess from the one-electron Hamiltonian"
    evals, evecs = geigh(h, S)
    return evecs


def get_nel(atoms, charge=0):
    print "Warning, hartree_fock.get_nel deprecated"
    print "Use the Molecular instance function"
    return atoms.get_nel(charge)


def get_enuke(atoms):
    print "Warning, hartree_fock.get_enuke deprecated"
    print "Use the Molecular instance function"
    return atoms.get_enuke()


def getJ_hartree(Ints, D):
    "Form the Coulomb operator corresponding to a density matrix D"
    nbf = D.shape[0]
    D1d = reshape(D, (nbf * nbf,))  # 1D version of Dens
    J = zeros((nbf, nbf), 'd')
    for i in xrange(nbf):
        for j in xrange(i + 1):
            temp = zeros_like(fetch_jints(Ints, i, j, nbf))
            if i != j:
                temp = fetch_jints(Ints, i, j, nbf)
            J[i, j] = dot(temp, D1d)
            J[j, i] = J[i, j]
    return J


def hf(atoms, **kwargs):
    """\
    Experimental inteface to rhf/uhf routines. See those
    routines for more information.
    """
    # Ultimately this will check spin_type to determine whether
    # to call uhf or rohf for open shell
    nclosed, nopen = atoms.get_closedopen()
    if nopen:
        return uhf(atoms, **kwargs)
    return rhf(atoms, **kwargs)


def hartree_product(atoms, **kwargs):
    """\
    rhf(atoms,**kwargs) - Closed-shell HF driving routine

    atoms       A Molecule object containing the molecule

    Options:      Value   Description
    --------      -----   -----------
    ConvCriteria  1e-4    Convergence Criteria
    MaxIter       20      Maximum SCF iterations
    DoAveraging   True    Use DIIS for accelerated convergence (default)
                  False   No convergence acceleration
    ETemp         False   Use ETemp value for finite temperature DFT (default)
                  float   Use (float) for the electron temperature
    bfs           None    The basis functions to use. List of CGBF's
    basis_data    None    The basis data to use to construct bfs
    integrals     None    The one- and two-electron integrals to use
                          If not None, S,h,Ints
    orbs          None    If not none, the guess orbitals
    """
    ConvCriteria = kwargs.get('ConvCriteria', settings.ConvergenceCriteria)
    MaxIter = kwargs.get('MaxIter', settings.MaxIter)
    DoAveraging = kwargs.get('DoAveraging', settings.Averaging)
    ETemp = kwargs.get('ETemp', settings.ElectronTemperature)

    logger.info("Hartree Product calculation on %s" % atoms.name)

    bfs = getbasis(atoms, **kwargs)

    nclosed, nopen = atoms.get_closedopen()
    nocc = nclosed
    assert(nopen == 0), "SCF currently only works for closed-shell systems"

    logger.info("Nbf = %d" % len(bfs))
    logger.info("Nclosed = %d" % nclosed)

    S, h, Ints = getints(bfs, atoms, **kwargs)
    nel = atoms.get_nel()

    orbs = kwargs.get('orbs')
    if orbs is None:
        orbe, orbs = geigh(h, S)

    enuke = atoms.get_enuke()
    eold = 0.

    if DoAveraging:
        logger.info("Using DIIS averaging")
        avg = DIIS(S)
    logging.debug("Optimization of Hartree Product orbitals")
    for i in xrange(MaxIter):
        if ETemp:
            efermi = get_efermi(nel, orbe, ETemp)
            occs = get_fermi_occs(efermi, orbe, ETemp)
            D = mkdens_occs(orbs, occs)
            entropy = get_entropy(occs, ETemp)
        else:
            D = mkdens(orbs, 0, nocc)
        G = 0.5 * getJ_hartree(Ints, D)
        F = h + G
        if DoAveraging:
            F = avg.getF(F, D)
        orbe, orbs = geigh(F, S)
        energy = get_energy(h, F, D, enuke)
        if ETemp:
            energy += entropy
        logging.debug("%d %f" % (i, energy))
        if abs(energy - eold) < ConvCriteria:
            break
        logger.info(
            "Iteration: %d    Energy: %f    EnergyVar: %f" %
            (i, energy, abs(
                energy - eold)))
        eold = energy
    if i < MaxIter:
        logger.info("PyQuante converged in %d iterations" % i)
    else:
        logger.warning("PyQuante failed to converge after %d iterations"
                       % MaxIter)
    logger.info(
        "Final Hartree Product energy for system %s is %f" %
        (atoms.name, energy))
    return energy, orbe, orbs


# added by Hatem H Helal hhh23@cam.ac.uk
def mk_auger_dens(c, occ):
    "Forms a density matrix from a coef matrix c and occupations in occ"
    # count how many states we were given
    nstates = occ.shape[0]
    D = 0.0
    for i in xrange(nstates):
        D += occ[i] * dot(c[:, i:i + 1], transpose(c[:, i:i + 1]))
    # pad_out(D)
    return D


if __name__ == '__main__':
    from .Molecule import Molecule
    h2 = Molecule('H2', atomlist=[(1, (1., 0, 0)), (1, (-1., 0, 0))])
    en = rhf(h2)
    print en
